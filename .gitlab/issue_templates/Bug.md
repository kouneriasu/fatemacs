# Summary

(Describe the bug encountered concisely.)




# How do you reproduce the behavior?

(List the steps required. Clarity and accuracy are important.




# In what version of the software did the problem occur?

(If you know, provide a version number, or, even better, a git ref.)



# What is the expected behavior?

(What should have happened?)



# What is the observed behavior?

(What actually happened?)



# Relevant logs, output, and/or screenshots

(Paste and relevant logs. Please use code blocks (```) to format console output, logs and code; it's hard to read otherwise.)



# Possible fixes

(If you can, link to the line(s) of code that might be responsible for the problem.)


/label ~bug ~needs-investigation
